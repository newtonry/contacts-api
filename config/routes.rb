Routes::Application.routes.draw do
  resources :users do
    resources :contacts, shallow: true, :only => [:index, :create, :show, :update, :destroy] do
      resources :comments, :only => [:index, :create, :show, :update, :destroy]
      member do
        post 'set_favorite'
      end
    end
    member do
      get 'favorites'
    end

    resources :groups, :only => [:index, :create, :show, :destroy]

  end




  resources :contact_shares, :only => [:create, :destroy]

  get '/' => 'users#index'
end
