class Group < ActiveRecord::Base
  attr_accessible :name, :user_id, :contact_id

  belongs_to :user
  belongs_to :contact

end
