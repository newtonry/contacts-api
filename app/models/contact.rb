class Contact < ActiveRecord::Base
  attr_accessible :name, :email, :user_id
  validates :name, :email, :user_id, :presence => true

  has_many :contact_shares
  has_many :shared_users, :through => :contact_shares, :source => :user
  has_many :groups
  has_many :comments
  belongs_to :user
end
