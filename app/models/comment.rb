class Comment < ActiveRecord::Base
  attr_accessible :body, :user_id, :contact_id
  validates :body, :user_id, :contact_id, :presence => true

  belongs_to :user
  belongs_to :contact
end
