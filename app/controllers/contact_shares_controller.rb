class ContactSharesController < ApplicationController

  def create
    @share = ContactShare.new(params[:share])
    if @share.save
      render :json => @share
    else
      render :json => @share.errors, :status => :unprocessable_entity
    end
  end

  def destroy
     if ContactShare.find(params[:id]).destroy
       render :json => true
     else
       render :json => false
     end
  end

end
