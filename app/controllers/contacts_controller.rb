class ContactsController < ApplicationController
  def index
    @user = User.find(params[:user_id])

    @contacts = @user.contacts + @user.shared_contacts
    render json: @contacts
  end

  def create
    @contact = User.find(params[:user_id]).contacts.new(params[:contact])

    if @contact.save
      render :json => @contact
    else
      render :json => @contact.errors, :status => :unprocessable_entity
    end
  end

  def show
    @contact = Contact.find(params[:id])
    render json: @contact
  end

  def update
    @contact = Contact.find(params[:id])
    @contact.update_attributes(params[:contact])
    if @contact.save
      render json: @contact
    else
      render :json => @contact.errors, :status => :unprocessable_entity
    end
  end

  def destroy
    if Contact.find(params[:id]).destroy
      render json: true
    else
      render json: false
    end
  end

  def set_favorite
    @contact = Contact.find(params[:id])
    @contact.favorite = params[:favorite]
    if @contact.save
      render json: @contact
    else
      render :json => @contact.errors, :status => :unprocessable_entity
    end
  end

end
