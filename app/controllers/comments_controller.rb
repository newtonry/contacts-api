class CommentsController < ApplicationController
  def index
    @comments = Comment.where('contact_id = ? AND user_id = ?',params[:contact_id],params[:user_id])
    render :json => @comments
  end

  def create
    @comment =  Contact.find(params[:contact_id]).comments.new(params[:comment])
    if @comment.save
      render :json => @comment
    else
      render :json => @comment.errors, :status => :unprocessable_entity
    end
  end

  def show
    @comment = Comment.find(params[:id])
    render :json => @comment
  end

  def update
    @comment = Comment.find(params[:id])
    @comment.update_attributes(params[:comment])

    if @comment.save
      render :json => @comment
    else
      render :json => @comment.errors, :status => :unprocessable_entity
    end
  end

  def destroy
    if Comment.find(params[:id]).destroy
      render :json => true
    else
      render :json => false
    end
  end
end
