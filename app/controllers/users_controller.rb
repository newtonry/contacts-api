class UsersController < ApplicationController
  def index
    render :json => User.all
  end

  def create
    @user = User.new(params[:@user])

    if @user.save
      render :json => @user
    else
      render :json => @user.errors, :status => :unprocessable_entity
    end
  end

  def show
    render :json => User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    @user.update_attributes(params[:user])

    if @user.save
      render :json => @user
    else
      render :json => @user.errors, :status => :unprocessable_entity
    end

  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      render :json => true
    else
      render :json => false
    end
  end

  def favorites
    @results = Contact.where("user_id = ? AND favorite = ?",params[:id],true)
    render :json => @results
  end
end
