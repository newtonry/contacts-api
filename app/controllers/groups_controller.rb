class GroupsController < ApplicationController
  def index
    @groups = User.find(params[:user_id]).groups
    render :json => @groups
  end

  def create
    @group =  User.find(params[:user_id]).groups.new(params[:group])

    if @group.save
      render :json => @group
    else
      render :json => @group.errors, :status => :unprocessable_entity
    end
  end

  def show
    @group = Group.where('id = ? AND user_id = ?',params[:id],params[:user_id])
    render :json => @group
  end

  def destroy
    if Group.find(params[:id]).destroy
      render :json => true
    else
      render :json => false
    end
  end


end



